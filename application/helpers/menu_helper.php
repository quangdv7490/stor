<?php
function recusive($data, $parent = 0, $text = "", $select = 0)
{
    foreach ($data as $key => $value) {
        if ($value['parent'] == $parent) {
            $id = $value['id'];
            if ($select != 0 && $id == $select) {
                echo "<option value='$value[id]' selected='selected'>" . $text . $value['cate_name'] . "</option>";
            } else {
                echo "<option value='$value[id]'>" . $text . $value['cate_name'] . "</option>";
            }
            unset($data[$key]);
            recusive($data, $id, $text . "- - ", $select);
        }
    }
}