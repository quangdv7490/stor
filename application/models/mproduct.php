<?php

class Mproduct extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function listAllProduct($offset, $start) {
        $q = $this->db->select("p_id,p_name,p_price,short_description,p_image,cate_name,created_date,products.status")
                ->limit($offset, $start)
                ->join("categories", "products.cate_id = categories.id")
                ->order_by("p_id desc")
                ->get("products");
        return $q->result_array();
    }

    public function countAll() {
        return $this->db->count_all("products");
    }

    public function addProduct($data_insert) {
        $this->db->insert("products", $data_insert);
        return $this->db->insert_id();
    }

    public function addImageProduct($resl_id, $data_img) {
        foreach ($data_img as $p) {
            $this->db->query('INSERT INTO product_image(m_name, p_id) VALUES ("' . $p['image'] . '",' . $resl_id . ')');
        }
    }

}
