<?php
class Mgroup extends CI_Model{
    protected $_table = "group";
    public function __construct(){
        parent::__construct();
    }

    public function listGroup()
    {
        $q = $this->db->order_by("id DESC")->get($this->_table);
        return $q->result_array();
    }
}