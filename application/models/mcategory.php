<?php

class Mcategory extends CI_Model {

    protected $_table = "categories";

    public function __construct() {
        parent::__construct();
    }

    public function listAllCate() {
        $q = $this->db->get($this->_table);
        return $q->result_array();
    }

    public function getCateById($id) {
        $q = $this->db->where("id", $id)->get($this->_table);
        return $q->row_array();
    }

    public function countAll() {
        return $this->db->count_all($this->_table);
    }

    public function addCate($data_insert) {
        $this->db->insert($this->_table, $data_insert);
    }

    public function editCate($data_update, $id) {
        $this->db->where("id", $id)->update($this->_table, $data_update);
    }

    public function deleteCate($id) {
        $this->db->where("id", $id)->delete($this->_table);
    }

}
