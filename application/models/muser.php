<?php

class Muser extends CI_Model
{
    protected $_table = "user";

    public function __construct()
    {
        parent::__construct();
    }

    public function countAll()
    {
        return $this->db->count_all($this->_table);
    }

    public function listAllUser($offset, $start)
    {
        $q = $this->db->limit($offset, $start)
            ->order_by("id desc")
            ->get($this->_table);
        return $q->result_array();
    }

    public function getUserById($id)
    {
        $q = $this->db->where("id", $id)->get($this->_table);
        return $q->row_array();
    }

    public function addUser($data_insert)
    {
        $this->db->insert($this->_table, $data_insert);
    }

    public function updateUser($data_update, $id)
    {
        $this->db->where("id", $id)
            ->update($this->_table, $data_update);
    }

    public function deleteUser($id)
    {
        $this->db->where("id", $id)->delete($this->_table);
    }

    public function check_username($user, $id)
    {
        $q = $this->db->where("username", $user)
            ->where("id !=", $id)
            ->get($this->_table);
        if ($q->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function check_email($email, $id)
    {
        $q = $this->db->where("email", $email)
            ->where("id !=", $id)
            ->get($this->_table);
        if ($q->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function checkLogin($u, $p)
    {
        $q = $this->db->where("username", $u)
            ->where("password", $p)
            ->get($this->_table);
        if ($q->num_rows() > 0) {
            return $q->row_array();
        } else {
            return false;
        }
    }
}