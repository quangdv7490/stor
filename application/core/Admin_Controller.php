<?php

class Admin_Controller extends MY_Controller
{
    protected $_data;

    public function __construct()
    {
        parent::__construct();
        $module = $this->uri->segment(1);
        $this->_data['module'] = $module;
        $this->_data['path'] = "$module/layout";

        /*Check rule user login*/
        if ($this->session->userdata('level') != 1 && $this->uri->segment(2) != "verify") {
            redirect(base_url() . 'admin/verify/login');
        }
    }
}