<?php

class Category extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        //load library form_validation
        $this->load->library("form_validation");
        //load model category
        $this->load->model("Mcategory");
        //load helper menu
        $this->load->helper("menu");
    }

    public function index() {
        $this->_data['titlePage'] = "Online-Stor::Category Manager";
        $this->_data['contentPage'] = "category/index_view";
        $this->_data['info'] = $this->Mcategory->listAllCate();
        $this->_data['mess'] = $this->session->flashdata("flash_mess");
        $this->load->view($this->_data['path'], $this->_data);
    }

    public function add() {
        $this->_data['titlePage'] = "Online-Stor::Create Category";
        $this->_data['contentPage'] = "category/add_view";
        $this->_data['menu'] = $this->Mcategory->listAllCate();
        
        $this->form_validation->set_rules('txtcatename', 'Category name', 'required');
        if ($this->form_validation->run() != FALSE) {
            $data_insert = array(
                "cate_name" => $this->input->post("txtcatename"),
                "parent" => $this->input->post("cate"),
                "status" =>  $this->input->post("status")
            );
            $this->Mcategory->addCate($data_insert);
            $this->session->set_flashdata("flash_mess", "Add New Category Successfully!");
            redirect(base_url() . 'admin/category');
        }
        
        $this->load->view($this->_data['path'], $this->_data);
    }
    
    public function edit() {
        $id = $this->uri->segment(4);
        $this->_data['titlePage'] = "Online-Stor::Edit Category";
        $this->_data['contentPage'] = "category/edit_view";
        $this->_data['menu'] = $this->Mcategory->listAllCate();
        $this->_data['info'] = $this->Mcategory->getCateById($id);
        $this->form_validation->set_rules('txtcatename', 'Category name', 'required');
        if ($this->form_validation->run() != FALSE) {
            $data_update = array(
                "cate_name" => $this->input->post("txtcatename"),
                "parent" => $this->input->post("cate"),
                "status" =>  $this->input->post("status")
            );
            $this->Mcategory->editCate($data_update,$id);
            $this->session->set_flashdata("flash_mess", "Update Category Successfully!");
            redirect(base_url() . 'admin/category');
        }
        $this->load->view($this->_data['path'], $this->_data);
    }

    public function del() {
        $id = $this->uri->segment(4);
        $this->Mcategory->deleteCate($id);
        $this->session->set_flashdata("flash_mess", "Delete Category Successfully!");
        redirect(base_url() . 'admin/category');
    }

}
