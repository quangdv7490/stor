<?php

class Product extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        //load library form validation
        $this->load->library("form_validation");


        //loadd model product
        $this->load->model("Mcategory");
        $this->load->model("Mproduct");

        //Change permission to false for KCFINDER - Requied
        session_start();
        $_SESSION['KCFINDER']['disabled'] = false;
    }

    public function index() {
        $this->_data['titlePage'] = "Online-Stor::Products List";
        $this->_data['contentPage'] = "product/index_view";

        $config['base_url'] = base_url() . 'admin/product/index';
        $config['total_rows'] = $this->Mproduct->countAll();
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $config['num_links'] = 2;

        $config['full_tag_open'] = '<ul class="pagination pull-right">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->load->library('pagination', $config);
        $offset = $config['per_page'];
        $start = $this->uri->segment(4);
        
        $this->_data['css'] = array("style1.css","style2.css");

        $this->_data['info'] = $this->Mproduct->listAllProduct($offset, $start);
        $this->_data['mess'] = $this->session->flashdata("flash_mess");
        $this->load->view($this->_data['path'], $this->_data);
    }

    public function add() {
        echo "<pre>";
        print_r($this->input->post());
        echo "</pre>";
        $this->load->helper("menu");
        $this->load->helper("ckeditor");
        $this->_data['titlePage'] = "Online-Stor::New Product";
        $this->_data['contentPage'] = "product/add_view";
        $this->_data['menu'] = $this->Mcategory->listAllCate();

        $this->form_validation->set_rules('productname', 'Product name', 'required');
        $this->form_validation->set_rules('productprice', 'Product price', 'required');
        $this->form_validation->set_rules('shortdesc', 'Short description', 'required');
        $this->form_validation->set_rules('fulldesc', 'Full description', 'required');
        $this->form_validation->set_rules('createddate', 'Created date', 'required');
        $checkImage = TRUE;
        if ($this->form_validation->run() != FALSE) {
            $data_insert = array(
                "p_name" => $this->input->post("productname"),
                "p_price" => $this->input->post("productprice"),
                "short_description" => $this->input->post("shortdesc"),
                "full_description" => $this->input->post("fulldesc"),
                "cate_id" => $this->input->post("cate"),
                "created_date" => date('Y-m-d', strtotime($this->input->post("createddate"))),
                "status" => $this->input->post("status")
            );

            if ($_FILES['pimage']['name'] != "") {
                $config['upload_path'] = './uploads/images/products/large/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '1000';
                $config['max_width'] = '1920';
                $config['max_height'] = '1080';
                $this->load->library('upload', $config);
                if ($this->upload->do_upload("pimage")) {
                    $image = $this->upload->data();
                    $data_insert['p_image'] = $image['file_name'];
                    //load library handle image upload - resize
                    $this->load->library("image_lib");
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './uploads/images/products/large/' . $image['file_name'];
                    $config['new_image'] = './uploads/images/products/';
                    $config['create_thumb'] = TRUE;
                    $config['thumb_marker'] = "";
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 160;
                    $config['height'] = 160;
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                } else {
                    $this->_data['error'] = $this->upload->display_errors();
                    $checkImage = FALSE;
                }
            }

            if ($checkImage) {
                $resl_id = $this->Mproduct->addProduct($data_insert);
                if ($resl_id > 0) {
                    if ($this->input->post("product_image") != "") {
                        $data_img = $this->input->post("product_image");
                        $this->Mproduct->addImageProduct($resl_id,$data_img);
                    }
                }
                $this->session->set_flashdata("flash_mess", "Add new product successfully!");
                redirect(base_url() . "admin/product");
            }
        }

        $this->load->view($this->_data['path'], $this->_data);
    }

}
