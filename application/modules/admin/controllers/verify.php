<?php

class Verify extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        //load library encryption
        $this->load->model("Muser");
    }

    public function login()
    {
        $this->_data['titlePage'] = "Online-Stor::Login";
        $this->_data['error'] = "";
        if ($this->input->post("ok")) {
            $u = $this->input->post("username");
            $p = md5($this->input->post("password"));
            $data_info = $this->Muser->checkLogin($u, $p);
            if ($data_info == false) {
                $this->_data['error'] = "Wrong username or password!";
            } else {
                $sess = array(
                    "id" => $data_info['id'],
                    "username" => $data_info['username'],
                    "level" => $data_info['group_id']
                );
                $this->session->set_userdata($sess);
                redirect(base_url() . 'admin/user/index');
            }
        }
        $this->load->view("verify/login_view", $this->_data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url() . 'admin/verify/login');
    }
}