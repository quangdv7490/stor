<?php

class User extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        //load library form_validation
        $this->load->library("form_validation");
        $this->form_validation->CI =& $this;

        //load model
        $this->load->model("Muser");
        $this->load->model("Mgroup");
    }

    public function index()
    {
        $this->_data['titlePage'] = "Online-Stor::User Manager";
        $this->_data['contentPage'] = "user/index_view";

        $config['base_url'] = base_url() . 'admin/user/index';
        $config['total_rows'] = $this->Muser->countAll();
        $config['per_page'] = 8;
        $config['uri_segment'] = 4;
        $config['num_links'] = 2;

        $config['full_tag_open'] = '<ul class="pagination pull-right">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '&raquo;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->load->library('pagination', $config);
        $offset = $config['per_page'];
        $start = $this->uri->segment(4);
        $this->_data['info'] = $this->Muser->listAllUser($offset, $start);
        $this->_data['mess'] = $this->session->flashdata('flash_mess');
        $this->load->view($this->_data['path'], $this->_data);
    }

    public function add()
    {
        $this->_data['titlePage'] = "Online-Stor::Add new user";
        $this->_data['contentPage'] = "user/add_view";

        //handle validation
        $this->form_validation->set_rules('txtfirstname', 'First name', 'required');
        $this->form_validation->set_rules('txtlastname', 'Last name', 'required');
        $this->form_validation->set_rules('txtuser', 'Username', 'required|min_length[4]|callback_username_check');
        $this->form_validation->set_rules('txtpassword', 'Password', 'required|matches[txtpassconf]');
        $this->form_validation->set_rules('txtpassconf', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('txtemail', 'Email', 'required|valid_email|callback_email_check');
        $this->form_validation->set_rules('txtbirth', 'Birthday', 'required');

        if ($this->form_validation->run() != false) {
            $pass = md5($this->input->post('txtpassword'));
            $birth = date('Y-m-d', strtotime($this->input->post('txtbirth')));
            $data_insert = array(
                "username" => $this->input->post('txtuser'),
                "email" => $this->input->post('txtemail'),
                "password" => $pass,
                "first_name" => $this->input->post('txtfirstname'),
                "last_name" => $this->input->post('txtlastname'),
                "birthday" => $birth,
                "status" => $this->input->post('status'),
                "group_id" => $this->input->post('group')
            );
            $this->Muser->addUser($data_insert);
            $this->session->set_flashdata('flash_mess', 'Add new user successfully!');
            redirect(base_url() . 'admin/user/index');
        }

        $this->_data['group'] = $this->Mgroup->listGroup();
        $this->load->view($this->_data['path'], $this->_data);
    }

    public function edit()
    {
        $id = $this->uri->segment(4);
        $this->_data['titlePage'] = "Online-Stor::Edit user";
        $this->_data['contentPage'] = "user/edit_view";

        //handle validation
        $this->form_validation->set_rules('txtfirstname', 'First name', 'required');
        $this->form_validation->set_rules('txtlastname', 'Last name', 'required');
        $this->form_validation->set_rules('txtuser', 'Username', 'required|min_length[4]|callback_username_check');
        $this->form_validation->set_rules('txtpassword', 'Password', 'matches[txtpassconf]');
        $this->form_validation->set_rules('txtpassconf', 'Password Confirmation', '');
        $this->form_validation->set_rules('txtemail', 'Email', 'required|valid_email|callback_email_check');
        $this->form_validation->set_rules('txtbirth', 'Birthday', 'required');

        if ($this->form_validation->run() != false) {
            $birth = date('Y-m-d', strtotime($this->input->post('txtbirth')));
            $data_update = array(
                "username" => $this->input->post('txtuser'),
                "email" => $this->input->post('txtemail'),
                "first_name" => $this->input->post('txtfirstname'),
                "last_name" => $this->input->post('txtlastname'),
                "birthday" => $birth,
                "status" => $this->input->post('status'),
                "group_id" => $this->input->post('group')
            );
            if ($this->input->post('txtpassword')) {
                $pass = md5($this->input->post('txtpassword'));
                $data_update['password'] = $pass;
            }
            $this->Muser->updateUser($data_update, $id);
            $this->session->set_flashdata('flash_mess', 'Update user successfully!');
            redirect(base_url() . 'admin/user/index');
        }

        $this->_data['group'] = $this->Mgroup->listGroup();
        $this->_data['info'] = $this->Muser->getUserById($id);
        $this->load->view($this->_data['path'], $this->_data);
    }

    public function del()
    {
        $id = $this->uri->segment(4);
        $this->Muser->deleteUser($id);
        $this->session->set_flashdata('flash_mess', 'Delete user successfully!');
        redirect(base_url() . 'admin/user/index');
    }

    public function username_check($user)
    {
        $id = $this->uri->segment(4);
        $check = $this->Muser->check_username($user,$id);
        if ($check == false) {
            $this->form_validation->set_message('username_check', 'This username registed. Please try again!');
            return false;
        } else {
            return true;
        }
    }

    public function email_check($email)
    {
        $id = $this->uri->segment(4);
        $check = $this->Muser->check_email($email,$id);
        if ($check == false) {
            $this->form_validation->set_message('email_check', 'This email registed. Please try again!');
            return false;
        } else {
            return true;
        }
    }

}