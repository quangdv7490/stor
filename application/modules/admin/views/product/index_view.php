
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Products Manager</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php if (isset($mess) && $mess != ""): ?>
            <div class="alert alert-success"><?php echo $mess; ?></div>
        <?php endif; ?>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="btn btn-default btn-sm" href="<?php echo base_url() . $module; ?>/product/add">
                    <span class="glyphicon glyphicon-plus"></span> Add new product
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th class="col-sm-1">Image</th>
                                <th class="col-sm-3">Name</th>
                                <th class="col-sm-1">Price</th>
                                <th class="col-sm-4">Short Description</th>
                                <th class="col-sm-2">Category</th>
                                <th class="col-sm-1">Post on</th>
                                <th>Status</th>
                                <th>Edit</th>
                                <th>Del</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($info as $items): ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td>
                                        <?php if ($items['p_image'] != ""): ?>
                                            <img src="<?php echo base_url(); ?>uploads/images/products/<?php echo $items['p_image']; ?>" alt="" />
                                        <?php else: ?>
                                            <img src="<?php echo base_url(); ?>uploads/images/products/no-image.jpg" alt="" />
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo $items['p_name']; ?></td>
                                    <td><?php echo $items['p_price'] . " VND"; ?></td>
                                    <td><?php echo $items['short_description']; ?></td>
                                    <td><?php echo $items['cate_name']; ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($items['created_date'])); ?></td>
                                    <?php if ($items['status'] == 1): ?>
                                        <td><span class="label label-default">Active</span></td>
                                    <?php else: ?>
                                        <td><span class="label label-warning">Inactive</span></td>
                                    <?php endif; ?>
                                    <td><a class="btn btn-primary btn-xs" href="<?php echo base_url() . $module; ?>/product/edit/<?php echo $items['p_id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                    <td><a class="btn btn-danger btn-xs" href="<?php echo base_url() . $module; ?>/product/del/<?php echo $items['p_id']; ?>" onclick="return confirm_delete('Are you sure delete this product?')"><span class="glyphicon glyphicon-trash"></span></a></td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer clearfix">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->