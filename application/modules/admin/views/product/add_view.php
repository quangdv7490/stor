<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add new product</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<form action="<?php echo base_url() . $module; ?>/product/add" method="post" enctype="multipart/form-data" id="form-product">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <a class="btn btn-default btn-sm" href="<?php echo base_url() . $module; ?>/product/index">
                        <span class="glyphicon glyphicon-circle-arrow-left"></span> Back to product list
                    </a>
                    <button type="submit" form="form-product" class="btn btn-primary btn-sm pull-right">
                        <span class="glyphicon glyphicon-floppy-disk"></span> Save
                    </button>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-9">
                            <?php echo validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
                        </div>
                    </div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#home" data-toggle="tab">Home</a></li>
                        <li><a href="#pimage" data-toggle="tab">Image</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane in active" id="home">
                            <div class="row sb-margin-top">
                                <div class="col-lg-9">
                                    <div class="form-group">
                                        <label>Categories</label>
                                        <select name="cate" class="form-control">
                                            <?php recusive($menu); ?> 
                                        </select>
                                    </div>
                                    <div class="form-group <?php echo (form_error('productname') ? 'has-error' : ''); ?>">
                                        <label>Product name</label>
                                        <input type="text" name="productname" class="form-control" value="<?php echo set_value('productname'); ?>">
                                        <?php echo form_error('productname', '<p class="help-block text-danger">', '</p>'); ?>
                                    </div>
                                    <div class="form-group <?php echo (form_error('productprice') ? 'has-error' : ''); ?>">
                                        <label>Price</label>
                                        <input type="text" name="productprice" class="form-control" value="<?php echo set_value('productprice'); ?>">
                                        <?php echo form_error('productprice', '<p class="help-block text-danger">', '</p>'); ?>
                                    </div>
                                    <div class="form-group <?php echo (form_error('shortdesc') ? 'has-error' : ''); ?>">
                                        <label>Short description</label>
                                        <textarea name="shortdesc" class="form-control" rows="5"><?php echo set_value('shortdesc'); ?></textarea>
                                        <?php echo form_error('shortdesc', '<p class="help-block text-danger">', '</p>'); ?>
                                    </div>
                                    <div class="form-group <?php echo (form_error('fulldesc') ? 'has-error' : ''); ?>">
                                        <label>Description</label>
                                        <textarea id="fulldesc" name="fulldesc"><?php echo set_value('fulldesc'); ?></textarea>
                                        <?php echo form_ckeditor(array('id' => 'fulldesc')); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Image</label>
                                        <input type="file" name="pimage">
                                    </div>
                                    <div class="form-group <?php echo (form_error('createddate') ? 'has-error' : ''); ?>">
                                        <label>createddate</label>
                                        <input id="createddate" type="text" name="createddate" class="form-control" value="<?php echo set_value('createddate'); ?>">
                                        <?php echo form_error('createddate', '<p class="help-block text-danger">', '</p>'); ?>
                                        <script type="text/javascript">
                                            $('#createddate').datepicker({
                                                format: 'dd/mm/yyyy'
                                            });
                                        </script>
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="1" checked>Active
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="0">Inactive
                                        </label>
                                    </div>

                                </div>
                                <!-- /.col-lg-9 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <div class="tab-pane" id="pimage">
                            <div class="row sb-margin-top">
                                <div class="col-lg-6">
                                    <div class="table-responsive">
                                        <table id="images" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td><button type="button" onclick="addImage()" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</form>
<!-- /.row -->

<script type="text/javascript">
    function openKCFinder(div) {
        window.KCFinder = {
            callBack: function (url) {
                window.KCFinder = null;
                div.innerHTML = '<div>Loading...</div>';
                var temp = url.split('/'); //convert url to array to get value image.
                var img = new Image();
                img.src = url;
                img.onload = function () {
                    div.innerHTML = '<img id="img" src="' + url + '" /><input type="hidden" name="product_image[][image]" value="' + temp[temp.length - 1] + '" />';
                    var img = document.getElementById('img');
                    var o_w = img.offsetWidth;
                    var o_h = img.offsetHeight;
                    var f_w = div.offsetWidth;
                    var f_h = div.offsetHeight;
                    if ((o_w > f_w) || (o_h > f_h)) {
                        if ((f_w / f_h) > (o_w / o_h))
                            f_w = parseInt((o_w * f_h) / o_h);
                        else if ((f_w / f_h) < (o_w / o_h))
                            f_h = parseInt((o_h * f_w) / o_w);
                        img.style.width = f_w + "px";
                        img.style.height = f_h + "px";
                    } else {
                        f_w = o_w;
                        f_h = o_h;
                    }
                    img.style.marginLeft = parseInt((div.offsetWidth - f_w) / 2) + 'px';
                    img.style.marginTop = parseInt((div.offsetHeight - f_h) / 2) + 'px';
                    img.style.visibility = "visible";
                }
            }
        };
        window.open('<?php echo base_url(); ?>/public/admin/js/ckeditor/kcfinder/browse.php?type=images&dir=images/products',
                'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
                'directories=0, resizable=1, scrollbars=0, width=1000, height=600'
                );
    }

    //var image_row = <?php //echo $image_row;    ?>;
    var image_row = 0;
    function addImage() {
        html = '<tr id="image-row' + image_row + '">';
        html += '<td><div id="image" class="img-thumbnail" onclick="openKCFinder(this)">Click here to choose an image</div></td>';
        html += '<td style="vertical-align: middle"><button type="button" onclick="$(\'#image-row' + image_row + '\').remove();" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#images tbody').append(html);
        image_row++;
    }
</script>

<style type="text/css">
    #image {
        width: 160px;
        height: 160px;
        overflow: hidden;
        cursor: pointer;
        color: #666;
        font-size: 10px;
    }
</style>