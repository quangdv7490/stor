<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">User Manager</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php if(isset($mess) && $mess != ""): ?>
            <div class="alert alert-success"><?php echo $mess; ?></div>
        <?php endif; ?>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="btn btn-default btn-sm" href="<?php echo base_url().$module; ?>/user/add">
                    <span class="glyphicon glyphicon-plus"></span> Add new user
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Birthday</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Del</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        <?php foreach($info as $items): ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $items['first_name']." ".$items['last_name']; ?></td>
                                <td><?php echo $items['username']; ?></td>
                                <td><?php echo $items['email']; ?></td>
                                <td><?php echo date('d-m-Y',strtotime($items['birthday'])); ?></td>
                                <?php if($items['group_id'] == 1): ?>
                                    <td><span class="label label-danger">Admin</span></td>
                                <?php else: ?>
                                    <td><span class="label label-info">Member</span></td>
                                <?php endif; ?>
                                <?php if($items['status'] == 1): ?>
                                    <td><span class="label label-default">Active</span></td>
                                <?php else: ?>
                                    <td><span class="label label-warning">Inactive</span></td>
                                <?php endif; ?>
                                <td><a class="btn btn-primary btn-xs" href="<?php echo base_url().$module; ?>/user/edit/<?php echo $items['id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                <td><a class="btn btn-danger btn-xs" href="<?php echo base_url().$module; ?>/user/del/<?php echo $items['id']; ?>" onclick="return confirm_delete('Are you sure delete this user?')"><span class="glyphicon glyphicon-trash"></span></a></td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer clearfix">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->