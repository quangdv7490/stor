<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit user</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="btn btn-default btn-sm" href="<?php echo base_url().$module; ?>/user/index">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span> Back to user list
                </a>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?php echo validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <form action="<?php echo base_url().$module; ?>/user/edit/<?php echo $info['id'] ?>" method="post" role="form">
                            <div class="form-group">
                                <label>Group</label>
                                <select name="group" class="form-control">
                                    <?php foreach($group as $opt): ?>
                                        <?php if($opt['id'] == $info['group_id']): ?>
                                            <option value="<?php echo $opt['id']; ?>" selected="selected"><?php echo $opt['group_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $opt['id']; ?>"><?php echo $opt['group_name']; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group <?php echo (form_error('txtfirstname')? 'has-error' : ''); ?>">
                                <label>First name</label>
                                <input type="text" name="txtfirstname" class="form-control" value="<?php echo $info['first_name']; ?>">
                                <?php echo form_error('txtfirstname','<p class="help-block text-danger">','</p>'); ?>
                            </div>
                            <div class="form-group <?php echo (form_error('txtlastname')? 'has-error' : ''); ?>">
                                <label>Last name</label>
                                <input type="text" name="txtlastname" class="form-control" value="<?php echo $info['last_name']; ?>">
                                <?php echo form_error('txtlastname','<p class="help-block text-danger">','</p>'); ?>
                            </div>
                            <div class="form-group <?php echo (form_error('txtuser')? 'has-error' : ''); ?>">
                                <label>Username</label>
                                <input type="text" name="txtuser" class="form-control" value="<?php echo $info['username']; ?>">
                                <?php echo form_error('txtuser','<p class="help-block text-danger">','</p>'); ?>
                            </div>
                            <div class="form-group <?php echo (form_error('txtpassword')? 'has-error' : ''); ?>">
                                <label>Password</label>
                                <input type="password" name="txtpassword" class="form-control">
                                <?php echo form_error('txtpassword','<p class="help-block text-danger">','</p>'); ?>
                            </div>
                            <div class="form-group <?php echo (form_error('txtpassconf')? 'has-error' : ''); ?>">
                                <label>Confirm password</label>
                                <input type="password" name="txtpassconf" class="form-control">
                                <?php echo form_error('txtpassconf','<p class="help-block text-danger">','</p>'); ?>
                            </div>
                            <div class="form-group <?php echo (form_error('txtemail')? 'has-error' : ''); ?>">
                                <label>Email</label>
                                <input type="text" name="txtemail" class="form-control" value="<?php echo $info['email']; ?>">
                                <?php echo form_error('txtemail','<p class="help-block text-danger">','</p>'); ?>
                            </div>
                            <div class="form-group <?php echo (form_error('txtbirth')? 'has-error' : ''); ?>">
                                <label>Birthday</label>
                                <input id="birth" type="text" name="txtbirth" class="form-control" value="<?php echo date('d/m/Y',strtotime($info['birthday'])); ?>">
                                <?php echo form_error('txtbirth','<p class="help-block text-danger">','</p>'); ?>
                                <script type="text/javascript">
                                    $('#birth').datepicker({
                                        format: 'dd/mm/yyyy'
                                    });
                                </script>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <?php if($info['status'] == 1): ?>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" value="1" checked>Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" value="0">Inactive
                                    </label>
                                <?php else: ?>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" value="1">Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" value="0" checked>Inactive
                                    </label>
                                <?php endif; ?>
                            </div>
                            <button type="submit" name="ok" class="btn btn-default">Submit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->