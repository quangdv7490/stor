<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add new category</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="btn btn-default btn-sm" href="<?php echo base_url() . $module; ?>/category/index">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span> Back to user list
                </a>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <?php echo validation_errors("<div class='alert alert-danger'>", "</div>"); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <form action="<?php echo base_url() . $module; ?>/category/edit/<?php echo $info['id']; ?>" method="post" role="form">
                            <div class="form-group">
                                <label>Categories</label>
                                <select name="cate" class="form-control">
                                    <option value="0">Root</option>
                                    <?php recusive($menu, 0, "", $info['parent']); ?>
                                </select>
                            </div>
                            <div class="form-group <?php echo (form_error('txtcatename') ? 'has-error' : ''); ?>">
                                <label>Category name</label>
                                <input type="text" name="txtcatename" class="form-control" value="<?php echo $info['cate_name']; ?>">
                                <?php echo form_error('txtcatename', '<p class="help-block text-danger">', '</p>'); ?>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <?php if ($info['status'] == 1): ?>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" value="1" checked>Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" value="0">Inactive
                                    </label>
                                <?php else: ?>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" value="1">Active
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" value="0" checked>Inactive
                                    </label>
                                <?php endif; ?>
                            </div>
                            <button type="submit" name="ok" class="btn btn-default">Submit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->