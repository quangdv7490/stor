<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Categories Manager</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php if(isset($mess) && $mess != ""): ?>
            <div id="mess-alert" class="alert alert-success"><?php echo $mess; ?></div>
        <?php endif; ?>
    </div>
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="btn btn-default btn-sm" href="<?php echo base_url().$module; ?>/category/add">
                    <span class="glyphicon glyphicon-plus"></span> Add new category
                </a>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Category Name</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Del</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        <?php foreach($info as $items): ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $items['cate_name']; ?></td>
                                <?php if($items['status'] == 1): ?>
                                    <td><span class="label label-default">Active</span></td>
                                <?php else: ?>
                                    <td><span class="label label-warning">Inactive</span></td>
                                <?php endif; ?>
                                <td><a class="btn btn-primary btn-xs" href="<?php echo base_url().$module; ?>/category/edit/<?php echo $items['id']; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                <td><a class="btn btn-danger btn-xs" href="<?php echo base_url().$module; ?>/category/del/<?php echo $items['id']; ?>" onclick="return confirm_delete('Are you sure delete this category?')"><span class="glyphicon glyphicon-trash"></span></a></td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
            <div class="panel-footer clearfix">
                
            </div>
            <!-- /.panel-footer -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
</div>
<!-- /.row -->