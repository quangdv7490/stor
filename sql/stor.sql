-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2014 at 12:36 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stor`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(10) unsigned NOT NULL,
  `cate_name` varchar(50) NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cate_name`, `parent`, `status`) VALUES
(1, 'electronics', 0, 1),
(2, 'clothes', 0, 1),
(3, 'food and beverages', 0, 1),
(4, 'health and beauty', 0, 1),
(5, 'Cameras', 1, 1),
(6, 'Computers, Tablets & Laptop', 1, 1),
(7, 'Mobile Phone', 1, 1),
(8, 'Sound & Vision', 1, 1),
(9, 'Women''s Clothing', 2, 1),
(10, 'Women''s Shoes', 2, 1),
(11, 'Women''s Hand Bags', 2, 1),
(12, 'Men''s Clothings', 2, 1),
(13, 'Men''s Shoes', 2, 1),
(14, 'Kids Clothing', 2, 1),
(15, 'Kids Shoes', 2, 1),
(16, 'Angoves', 3, 1),
(17, 'Bouchard Aine & Fils', 3, 1),
(18, 'French Rabbit', 3, 1),
(19, 'Louis Bernard', 3, 1),
(20, 'BIB Wine (Bag in Box)', 3, 1),
(21, 'Other Liquors & Wine', 3, 1),
(22, 'Garden', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE IF NOT EXISTS `group` (
`id` int(10) unsigned NOT NULL,
  `group_name` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `group_name`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`p_id` int(10) unsigned NOT NULL,
  `p_name` varchar(255) NOT NULL,
  `p_price` decimal(10,0) NOT NULL,
  `short_description` text NOT NULL,
  `full_description` longtext NOT NULL,
  `p_image` varchar(255) NOT NULL,
  `cate_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `p_name`, `p_price`, `short_description`, `full_description`, `p_image`, `cate_id`, `created_date`, `status`) VALUES
(1, 'KTS Cyber-shot', '12000000', 'Không chỉ hiện diện với thiết kế nhỏ-gọn-thời trang và đầy lịch lãm, mà ẩn bên trong là hiệu suất chụp ảnh vượt bật được tích hợp các tính năng như máy ảnh chuyên nghiệp.', '', '1.jpg', 5, '2014-10-09', 1),
(2, 'SMART CAMERA NX1000', '8000000', 'Samsung NX1000 với Smart Auto 2.0 tự động phân tích môi trường chụp ảnh và chọn chế độ phù hợp để cho kết quả đẹp nhất. Smart Auto 2.0 tính toán các yếu tố chính và tự động điều chỉnh theo chế độ 16 hình ảnh và 4 chế độ quay phim để có được hình ảnh tốt nhất', '', '2.jpg', 5, '2014-10-09', 1),
(3, 'dsfa', '324124', 'dswf', '<p>dasfa</p>\r\n', '', 7, '1970-01-01', 1),
(4, 'aaa', '12000000', 'aaa', '<p>aaa</p>\r\n', '', 1, '1970-01-01', 1),
(5, 'bbb', '13000000', 'bbb', '<p>bbb</p>\r\n', 'lotus.jpg', 7, '1970-01-01', 1),
(6, 'hhh', '111', 'hhh', '<p>hhh</p>\r\n', 'Dog-and-Cat-Wallpaper-teddybear64-16834786-1280-800.jpg', 7, '1970-01-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(10) unsigned NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `first_name`, `last_name`, `birthday`, `status`, `group_id`) VALUES
(1, 'quangdv', 'quangdv.90@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Đoàn', 'Quang', '1990-07-04', 1, 1),
(2, 'quannd', 'quannd@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 'Nguyễn', 'Quân', '1970-01-01', 1, 1),
(3, 'vietlh', 'vietlh@gmail.com', 'awkL5MYrcS3K7qsXIaCHQhCUrZLR7bQgmLYcHGEObgMi/2lFh/', 'Lương', 'Hoàng Việt', '1970-01-01', 1, 2),
(12, 'hungnq', 'hungnq@yahoo.com.vn', 'oU/LLsTlG1WGYBTBTsxD0iE/ECv6ntKozq7JNlhRPBJsm/NtY6', 'Nguyễn', 'Quang Hưng', '1990-07-04', 1, 2),
(13, 'loandt', 'loandt@gmail.com', 'SXCpnmlyllBmmWuJqvG2pjqR3koPWzxTHhcPrCOnn5VkVbyGPF', 'Đặng', 'Thị Loan', '1989-02-12', 1, 2),
(16, 'hailm', 'hailm@yahoo.com.vn', 'ZaPvY0j5/0nJM7l2UmCnRuWUNV9/sbacEoJt6VzmoIZolK+y0O', 'Luong', 'Manh Hai', '1982-12-09', 1, 2),
(17, 'hungnp', 'hungnp@gmail.com', 'dVF5hMBpgJhvgwVfi842WLh+HEiRlqwqqv7ICl+4VUg1S4IWlv', 'Nguyễn', 'Hùng', '1984-05-11', 1, 2),
(18, 'daitq', 'quangdai@gmail.com', 'R+sEwrcII7t5DoBdtctkxzIS/F2oiFy8zLSHjxo4gdVaO6MPA0', 'Trần', 'Quang Đại', '1970-01-01', 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `p_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
